// import { Component } from '@angular/core';
import { Component, OnInit, Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";

class Customer {
  id : number;
  // f1: string;
  // f2: string;
  time: string;
  title: string;
}

@Component({
  // sholeh remove:
  selector: 'app-root',
  // selector: 'customers',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

@Injectable()

// export class AppComponent implements OnInit {
export class AppComponent {
  // title = 'test-project';
  records = {};
  
  // customersObservable : Observable<Customer[]>;
  customersObservable : Observable<any>;

  constructor(private httpClient:HttpClient) {}

  ngOnInit() {

    // let headers = new HttpHeaders().set('Authorization','auth-token');
    let headers = new HttpHeaders().set('Access-Control-Allow-Origin', '*');

    // this.customersObservable = this.httpClient
    //      .get<Customer[]>("http://192.168.56.104:8000/api")
    //     // .get<Customer[]>("http://192.168.56.104:8000/getList")
    //       console.log("we got")
    //      ;

    this.customersObservable = this.httpClient.get('http://192.168.56.104:8000/getData',{headers});

    // this.customersObservable = this.httpClient.get('https://jsonplaceholder.typicode.com/posts',{headers});
    // this.customersObservable = this.httpClient.get('http://192.168.56.104:8000/api', {headers});
  }

  
}
